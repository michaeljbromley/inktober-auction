import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileLogger } from './file-logger';
import { DatabaseService } from './database.service';
import { EmailService } from './email.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, FileLogger, DatabaseService, EmailService],
})
export class AppModule {}
