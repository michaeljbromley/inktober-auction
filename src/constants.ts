import path from "path";

export const DB_FILE = path.join(__dirname, '../bids.sl3');
export const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD || `5f78742907858ceba07ba4d9f2ba5099cc1529829126c99278bdbaa64a9aaa8e`;
export const VERIFY_ROUTE = `verify-submission`;
export const VERIFY_URL = process.env.VERIFY_URL || `http://localhost:3000/`;
export const FROM_EMAIL = process.env.VERIFY_FROM_EMAIL || `no-reply@artsupplies.co.uk`;
export const MAIL_TRANSPORT: 'mailtrap' | 'sendmail' | 'mailgun' = process.env.EMAIL_TRANSPORT as any || 'mailtrap';
export const MAILGUN_USERNAME = process.env.MAILGUN_USERNAME;
export const MAILGUN_PASSWORD = process.env.MAILGUN_PASSWORD;
export const END_DATE = process.env.END_DATE || new Date('2020-11-30T12:00:00.000Z');
