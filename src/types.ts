export interface Submission {
    name: string;
    emailAddress: string;
    bidValue: string;
    town: string;
    postcode: string;
}

export interface Bid {
    id: number;
    name: string;
    emailAddress: string;
    town: string;
    postcode: string;
    bidValue: string;
    ip: string;
    time: string;
    verificationCode: string;
    verified: boolean;
}

export type PartialBid = Omit<Bid, 'id' | 'verified' | 'time'>;
