import { Controller, Get, Post, Render, Body, Req, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { Submission } from './types';
import { VERIFY_ROUTE, ADMIN_PASSWORD, END_DATE } from './constants';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get()
    @Render('index')
    root() {
        const topBids = this.appService.getTopBids()
        const topBid = topBids[0]?.bidValue ?? 0;
        const minBid = topBid + 1;
        const totalBids = this.appService.getTotalBidCount();
        return {
            bids: topBids,
            minBid,
            topBid,
            totalBids,
            endDate: END_DATE,
        };
    }

    @Get('terms')
    @Render('terms')
    terms() {
        return;
    }

    @Post('delete-bid')
    @Render('admin')
    deleteBid(@Body() body: any) {
        const {pw, id} = body;
        if (!pw || pw !== ADMIN_PASSWORD) {
            return {authorized: false};
        }
        this.appService.deleteBid(id);
        const bids = this.appService.getAllBids();
        return {authorized: true, bids, pw};
    }

    @Post('submit')
    @Render('submit')
    submit(@Body() submission: Submission, @Req() req: any) {
        return this.appService.handleSubmission(submission, req.clientIp)
    }

    @Get(VERIFY_ROUTE)
    @Render('verify')
    verify(@Query('code') code: string) {
        return this.appService.verifyBid(code);
    }

    @Get('view-bids')
    @Render('admin')
    viewBids() {
        return {authorized: false};
    }

    @Post('view-bids')
    @Render('admin')
    viewBidsResults(@Body('password') password: string) {
        if (!password || password !== ADMIN_PASSWORD) {
            return {authorized: false};
        }
        const bids = this.appService.getAllBids();
        return {authorized: true, bids, pw: password};
    }
}
