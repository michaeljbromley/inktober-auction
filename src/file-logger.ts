import { Injectable, Logger } from '@nestjs/common';
import fs from 'fs';
import path from 'path';

@Injectable()
export class FileLogger extends Logger {
    private readonly logFilePath: string;
    constructor(context?: string, isTimestampEnabled?: boolean) {
        super(context, isTimestampEnabled);
        this.logFilePath = path.join(__dirname, '../error.log');
    }

    error(message: any, trace?: string, context?: string) {
        this.logError(message);
        if (trace) {
            this.logError(trace);
        }
        super.error(message, trace, context);
    }

    private logError(message: string) {
        fs.appendFileSync(this.logFilePath, `[${new Date().toISOString()}]\t${message}\n`);
    }
}
