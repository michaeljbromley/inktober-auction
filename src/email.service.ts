import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import Mail from 'nodemailer/lib/mailer';
import nodemailer from 'nodemailer';
import { readFileSync } from "fs";
import Handlebars from 'handlebars';
import path from "path";
import { FROM_EMAIL, MAIL_TRANSPORT, MAILGUN_PASSWORD, MAILGUN_USERNAME, VERIFY_ROUTE, VERIFY_URL } from './constants';
import { PartialBid } from './types';

@Injectable()
export class EmailService implements OnApplicationBootstrap {
    transport: Mail;

    onApplicationBootstrap(): any {
        switch (MAIL_TRANSPORT) {
            case 'mailtrap':
                this.transport = nodemailer.createTransport({
                    host: "smtp.mailtrap.io",
                    port: 2525,
                    auth: {
                        user: "4e8bfd6beff474",
                        pass: "4d338af7833ad3"
                    }
                });
                break;
            case 'sendmail':
                this.transport = nodemailer.createTransport({
                    sendmail: true,
                    newline: 'unix',
                    args: ['-f', FROM_EMAIL, '-bs'],
                });
                break;
            case 'mailgun':
                this.transport = nodemailer.createTransport({
                    host: "smtp.eu.mailgun.org",
                    port: 587,
                    auth: {
                        user: MAILGUN_USERNAME,
                        pass: MAILGUN_PASSWORD,
                    }
                });
                break;
        }
    }

    async sendVerificationEmail(bid: PartialBid) {
        const emailText = readFileSync(path.join(__dirname, '../views/email.hbs'), 'utf-8');
        const template = Handlebars.compile(emailText);
        const verifyUrl = `${VERIFY_URL}${VERIFY_ROUTE}?code=${bid.verificationCode}`;
        const html = template({
            name: bid.name,
            bidValue: bid.bidValue,
            verifyUrl,
        })

        await this.transport.sendMail({
            from: FROM_EMAIL,
            to: bid.emailAddress,
            subject: `Please confirm your auction bid`,
            html,
        });
    }
}
