import { Injectable } from '@nestjs/common';
import { PartialBid, Submission, Bid } from './types';
import { FileLogger } from './file-logger';
import { DatabaseService } from './database.service';
import { EmailService } from './email.service';

@Injectable()
export class AppService {

    constructor(private logger: FileLogger, private databaseService: DatabaseService, private emailService: EmailService) {    }

    getTopBids(): Array<{ time: string; bidValue: number; town: string; initial: string; }> {
        return this.databaseService.getTopBids().map(row => ({
            bidValue: row.bidValue,
            town: row.town,
            time: row.time,
            initial: row.name.split(" ").map((n)=>n[0]).join(" "),
        }));
    }

    getTotalBidCount(): number {
        return this.databaseService.getTotalBidCount();
    }

    getAllBids(): Array<Bid> {
        return this.databaseService.getAllBids();
    }

    deleteBid(id: number) {
        return this.databaseService.deleteBid(id);
    }

    async handleSubmission(submission: Submission, ip: string) {
        const validationError = this.validateSubmission(submission);
        if (validationError) {
            return {success: false, error: validationError};
        }
        const bid: PartialBid = {
            ...submission,
            ip,
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            verificationCode: require('nanoid').nanoid(),
        }

        if (this.databaseService.bidAlreadyExists(bid)) {
            // If page is refreshed, do not make any new entries.
            return {success: true, emailAddress: bid.emailAddress, bidValue: bid.bidValue};
        }

        try {
            await this.emailService.sendVerificationEmail(bid);
            this.databaseService.insertBid(bid);
        } catch (e) {
            this.logger.error(e.message, e.stack);
            return {success: false, error: e.message};
        }
        return {success: true, emailAddress: bid.emailAddress, bidValue: bid.bidValue};
    }

    async verifyBid(code: string) {
        try {
            const match = this.databaseService.getBidByVerificationCode(code);
            if (match) {
                if (match.verified) {
                    // noop if already verified
                    return { success: true, bidValue: match.bidValue }
                } else {
                    this.databaseService.verifyBid(match.id);
                    return {success: true, bidValue: match.bidValue};
                }
            }
        } catch (e) {
            this.logger.error(e.message, e.stack);
        }
        return {success: false};
    }

    private validateSubmission(submission: Submission): string | undefined {
        if (!submission) {
            return 'No submission data was detected';
        }
        const required: (keyof Submission)[] = ['name', 'bidValue', 'emailAddress', 'town', 'postcode'];
        for (const prop of required) {
            if (!submission[prop]) {
                return `The required field ${prop} was not received`;
            }
        }
        const maxPossibleBid = this.databaseService.getMaxPossibleBid();
        if (maxPossibleBid < submission.bidValue) {
            return `The maximum you may bid is £${maxPossibleBid}`;
        }
    }
}
