import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import SQLiteDB, { Database } from 'better-sqlite3';
import { DB_FILE } from './constants';
import { FileLogger } from './file-logger';
import { Bid, PartialBid } from './types';

@Injectable()
export class DatabaseService implements OnApplicationBootstrap {
    db: Database;

    constructor(private logger: FileLogger) {
    }

    onApplicationBootstrap(): any {
        this.db = new SQLiteDB(DB_FILE);
        const stmt = this.db.prepare(`CREATE TABLE IF NOT EXISTS bids (
                id INTEGER PRIMARY KEY AUTOINCREMENT, 
                ip TEXT NOT NULL,
                time TEXT NOT NULL,
                name TEXT NOT NULL,
                emailAddress TEXT NOT NULL,
                town TEXT NOT NULL,
                postcode TEXT NOT NULL,
                verificationCode TEXT NOT NULL,
                verified INTEGER NOT NULL DEFAULT 0,
                bidValue REAL NOT NULL
                );`)
        stmt.run();
    }

    getTopBids(): Array<{ time: string, bidValue: number; name: string; town: string;  }> {
        const stmt = this.db.prepare(`SELECT time, bidValue, name, town FROM bids WHERE verified = 1 ORDER BY bidValue DESC, time ASC LIMIT 3`);
        const results = stmt.all();
        return results;
    }

    getTotalBidCount(): number {
        const stmt = this.db.prepare(`SELECT COUNT(*) as count from bids WHERE verified = 1`);
        const result = stmt.get();
        return result?.count ?? 0;
    }

    getAllBids(): Array<Bid> {
        const stmt = this.db.prepare(`SELECT * FROM bids ORDER BY bidValue DESC, time ASC LIMIT 5`);
        const results = stmt.all();
        return results;
    }

    insertBid(bid: PartialBid) {
        const stmt = this.db.prepare(`INSERT INTO bids (name, emailAddress, town, postcode, bidValue, ip, verificationCode, time) 
                                      VALUES (?, ?, ?, ?, ?, ?, ?, ?)`);
        stmt.run([bid.name, bid.emailAddress, bid.town, bid.postcode, bid.bidValue, bid.ip, bid.verificationCode, (new Date()).toISOString()]);
    }

    deleteBid(id: number) {
        const stmt = this.db.prepare(`DELETE FROM bids WHERE id = ?`);
        stmt.run([id]);
    }

    getBidByVerificationCode(code: string): Bid | undefined {
        const stmt = this.db.prepare(`SELECT * FROM bids WHERE verificationCode = ?`);
        return stmt.get([code]);
    }

    verifyBid(id: number) {
        const stmt = this.db.prepare(`UPDATE bids SET verified = 1 WHERE id = ?`);
        stmt.run([id]);
    }

    getMaxPossibleBid() {
        const stmt = this.db.prepare(`SELECT time, bidValue FROM bids WHERE verified = 1 ORDER BY bidValue DESC LIMIT 1`);
        const topBid = stmt.get();
        return (topBid?.bidValue ?? 0) + 200;
    }

    bidAlreadyExists(bid: PartialBid): boolean {
        const stmt = this.db.prepare(`SELECT time, bidValue, emailAddress FROM bids WHERE bidValue = ? AND emailAddress = ?`);
        const existing = stmt.get([bid.bidValue, bid.emailAddress]);
        return !!existing
    }
}
